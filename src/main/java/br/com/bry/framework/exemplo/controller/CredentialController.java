package br.com.bry.framework.exemplo.controller;

import br.com.bry.framework.exemplo.dto.CredentialResponse;
import br.com.bry.framework.exemplo.service.CredentialService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CredentialController {

    private final CredentialService service;

    @Autowired
    CredentialController(CredentialService service){
        this.service = service;
    }

    @GetMapping("jwt")
    public CredentialResponse generateAccessToken(){
        return this.service.generateAccessToken();
    }

}
