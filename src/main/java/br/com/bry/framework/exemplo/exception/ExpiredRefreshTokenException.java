package br.com.bry.framework.exemplo.exception;

import org.springframework.web.bind.annotation.ExceptionHandler;

public class ExpiredRefreshTokenException extends Throwable {

    private static final String message = "Submitted invalid (expired) refresh token.";

    public ExpiredRefreshTokenException(){
        super(message);
    }

}
