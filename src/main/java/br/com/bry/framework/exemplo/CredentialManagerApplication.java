package br.com.bry.framework.exemplo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.TimeZone;

@SpringBootApplication
public class CredentialManagerApplication {

    public static void main(String[] args) {
        // Set the default TimeZone of the Java virtual machine.
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(CredentialManagerApplication.class, args);
    }

}
