package br.com.bry.framework.exemplo.service;

import br.com.bry.framework.exemplo.dto.CredentialResponse;
import br.com.bry.framework.exemplo.exception.ExpiredRefreshTokenException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Map;

@Service
@EnableScheduling
public class ScheduledTaskService {

    private static final Log logger = LogFactory.getLog(ScheduledTaskService.class);
    // 60000L -> 1 minute.
    public static final long CHECK_INTERVAL = 60000L;
    private final CredentialService credentialService;

    @Autowired
    ScheduledTaskService(CredentialService service) {
        this.credentialService = service;
    }

    /**
     *  PostConstruct method responsible for initialize the credential that will be stored in memory.
     */
    @PostConstruct
    private void initializeApplicationCredential() {
        this.credentialService.generateAccessToken();
    }


    /**
     *
     * Method responsible for periodically validate the in-memory BRy Cloud platform credential.
     *
     */
    @Scheduled(fixedDelay = CHECK_INTERVAL)
    public void checkCredentialExpiration() {

        // Recover from service the current in-memory credential reference.
        CredentialResponse credentialResponse = this.credentialService.generateAccessToken();

        // Parses the access token (JWT) string.
        DecodedJWT decode = JWT.decode(credentialResponse.getAccessToken());
        Map<String, Claim> claims = decode.getClaims();

        // From the claims list, obtain the expiration time and converts to the comparison representation.
        Claim exp = claims.get("exp");
        long expDateTransformed = exp.asLong() * 1000;

        // Obtain the current time in milliseconds to the comparison.
        long currentTimeInMillis = OffsetDateTime.now(ZoneOffset.UTC).toInstant().toEpochMilli();
        
        if(expDateTransformed <= currentTimeInMillis + CHECK_INTERVAL) {
            logger.info("Starting access token renew.");
            try {
                this.credentialService.renewAccessToken(credentialResponse.getRefreshToken());
            }catch(ExpiredRefreshTokenException e) {
                logger.error("Error while operation of access token renew. Refresh Token is expired.");
            }
        }else{
            logger.info("Remaining time to use current access token: " + ((expDateTransformed - currentTimeInMillis)/60000) + " minutos");
        }

    }

}
