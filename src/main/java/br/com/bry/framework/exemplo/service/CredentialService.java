package br.com.bry.framework.exemplo.service;

import br.com.bry.framework.exemplo.dto.CredentialResponse;
import br.com.bry.framework.exemplo.exception.ExpiredRefreshTokenException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;

@Component
public class CredentialService {

    @Value("${oauth2.provider.address}")
    private String address;
    @Value("${oauth2.credentials.client-id}")
    private String clientId;
    @Value("${oauth2.credentials.client-secret}")
    private String clientSecret;

    private CredentialResponse currentCredential;

    public synchronized CredentialResponse generateAccessToken(){

        if(this.currentCredential != null){
            long currentTimeInMillis = OffsetDateTime.now(ZoneOffset.UTC).toInstant().toEpochMilli();

            DecodedJWT decode = JWT.decode(this.currentCredential.getAccessToken());

            // Moment to expire
            long expTime = decode.getClaims().get("exp").asLong() * 1000;

            if(expTime <= currentTimeInMillis) {
                this.currentCredential = postAccessTokenRequest();
            }
        }else{
            this.currentCredential = postAccessTokenRequest();
        }


        return this.currentCredential;
    }

    public CredentialResponse renewAccessToken(String refreshToken) throws ExpiredRefreshTokenException {

        DecodedJWT decodedRefreshToken = JWT.decode(refreshToken);

        long expTime = decodedRefreshToken.getClaims().get("exp").asLong() * 1000;
        long currentTimeInMillis = OffsetDateTime.now(ZoneOffset.UTC).toInstant().toEpochMilli();

        if(expTime <= currentTimeInMillis) {
            throw new ExpiredRefreshTokenException();
        }

        this.currentCredential = postRenewRequest(refreshToken);

        return this.currentCredential;

    }

    private CredentialResponse postAccessTokenRequest() {
        RestTemplate restTemplate = new RestTemplate();

        MappingJackson2HttpMessageConverter mappingJackson2HttpMessageConverter = new MappingJackson2HttpMessageConverter();
        mappingJackson2HttpMessageConverter.setSupportedMediaTypes(Arrays.asList(MediaType.APPLICATION_JSON, MediaType.APPLICATION_OCTET_STREAM, MediaType.APPLICATION_FORM_URLENCODED));
        restTemplate.getMessageConverters().add(mappingJackson2HttpMessageConverter);

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", "client_credentials");
        map.add("client_id", clientId);
        map.add("client_secret", clientSecret);

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

        return restTemplate.exchange(address, HttpMethod.POST, entity, CredentialResponse.class).getBody();
    }

    private CredentialResponse postRenewRequest(String refreshToken) {
        RestTemplate restTemplate = new RestTemplate();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type","refresh_token");
        map.add("refresh_token", refreshToken);

        HttpEntity<MultiValueMap<String, String>> entity = new HttpEntity<>(map, headers);

        return restTemplate.exchange(address, HttpMethod.POST, entity, CredentialResponse.class).getBody();
    }
}
