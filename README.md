# Gestão de validade da credencial de acesso

Este projeto apresenta uma abordagem de gerenciamento do tempo de vida das credenciais emitidas pela plataforma da BRy ([BRy Cloud]).

A abordagem apresenta um gerenciamento ativo, onde uma tarefa realiza validações períodicas do período de vigência do access token armazenado em memória.

O access token armazenado em memória válido e monitorado é disponibilizado na forma de serviço através do endereço: http://localhost:8080/jwt

Abaixo seguem os passos principais:
* Ao iniciar, a aplicação exemplo requisita um **Access Token** para a plataforma de serviços da BRy, utilizando as credenciais de uma aplicação cadastrada: **client_id** e **client_secret**.  
* Em um período configurável, a tarefa desperta e realiza a validação do tempo de vida do Access Token (processo de decodifição do JWT e extração da claim **exp**).
* Sempre que o Access Token estiver próximo de sua expiração, a tarefa dispara o evento de renovação da credencial utilizando o **Refresh Token**.

### Tech

O exemplo utiliza as seguintes tecnologias e bibliotecas Java abaixo:
* [Spring Boot] - Spring Boot
* [JDK 8] - Java 8
* [Java JWT] - A Java implementation of JSON Web Token (JWT) - RFC 7519.

### Variáveis que devem ser configuradas

O exemplo necessita ser configurado com os dados de conexão e identificação da aplicação cliente.

| Variável | Descrição | Arquivo |
| ------ | ------ | ------ |
| oauth2.provider.address | Endereço do endpoint de obtenção de credenciais. | application.properties
| oauth2.credentials.client-id | **client_id** de uma aplicação. | application.properties
| oauth2.credentials.client-secret | **client_secret** de uma aplicação. | application.properties

### Observações

Os atributos de data e hora codificados nos tokens JWT (access token e refresh token), seguem o fuso horário UTC-0.
Lembrando que o Brasil possui 4 fusos horários:
* UTC-2 - Atol das Rocas e Fernando de Noronha.
* UTC-3 **(oficial)** - Estados não citados nos demais itens.
* UTC-4 - Amazonas, Mato Grosso, Mato Grosso do Sul, Rondônia e Roraima.
* UTC-5 - Acre e alguns municípios do Amazonas.


### Uso

Para execução escolha sua IDE de preferência ou execute o projeto Spring Boot por linha de comando, executando os comando abaixo na pasta do projeto:

    mvn clean install
    java -jar <aplication.name>.jar


 [BRy Cloud]: <https://cloud.bry.com.br>
 [Spring Boot]: <https://spring.io/projects/spring-boot>
 [JDK 8]: <https://www.oracle.com/java/technologies/javase-jdk8-downloads.html>
 [Java JWT]: <https://github.com/auth0/java-jwt>
